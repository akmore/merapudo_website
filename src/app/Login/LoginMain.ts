import { Component, OnInit } from '@angular/core';
import { LoginController } from './LoginController';
import { LoginURLConstants } from './LoginURLConstants';
import { LoginAPICalls } from './LoginWebApi';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { variable } from '@angular/compiler/src/output/output_ast';
import { LoginRequestModel } from './Models/LoginModel';
import { RouterModule, Routes, Router } from '@angular/router';
declare var $: any;
@Component({
    selector: 'app-root',
    templateUrl: '../../assets/template/login.html',
    styleUrls: ['../../assets/css/login.css'],
    providers: [LoginAPICalls]
})
export class LoginMain implements OnInit {
    public login_form: FormGroup
    private loginController: LoginController;
    public loginRequestModel: any;
    constructor(private loginWebApi: LoginAPICalls, private router: Router) { }

    ngOnInit() {
        this.loginController = new LoginController(this.loginWebApi, this, this.router)
        this.login_form = new FormGroup({
            userName: new FormControl('', Validators.required),
            userPass: new FormControl('', Validators.required)
        });
        this.loginRequestModel = new LoginRequestModel()
    }

    public loginUser() {
        this.loginController.loginUser(this.login_form.get('userName').value,
            this.login_form.get('userPass').value)
    }
}