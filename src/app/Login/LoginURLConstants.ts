import { URLConstants } from '../UrlConstants/UrlConstant'

export class LoginURLConstants {

    public static checkUserLogin = URLConstants.awsMachineUrl + "checkLogin"
}