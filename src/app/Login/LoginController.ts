import { LoginMain } from './LoginMain'
import { LoginAPICalls } from './LoginWebApi'
import { LoginRequestModel } from './Models/LoginModel'
import { RouterModule, Routes, Router } from '@angular/router';
declare var $: any;

export class LoginController {
    constructor(private loginAPI: LoginAPICalls, private loginMain: LoginMain, private router: Router) {

    }

    public loginUser(userName, userPass) {
        this.loginMain.loginRequestModel.user_name = userName
        this.loginMain.loginRequestModel.user_pass = userPass
        $("#LoadingModalWithGif").modal("show");
        this.loginAPI.loginUser(this.loginMain.loginRequestModel).subscribe(result => {
            console.log("result api", result)
            try {
                if (result.status == "FAILURE") {
                    $("#LoadingModalWithGif").modal("hide")
                    $("#loginFailure").text(result.responseMessage)
                    this.router.navigate(['dashboard'])
                    console.log("failure")

                }
                else {


                    $("#LoadingModalWithGif").modal("hide")
                    console.log("success")

                }

            }
            catch (e) {
                //catch exception
                $("#LoadingModalWithGif").modal("hide")
                console.log("exception")

            }
        })


    }
}