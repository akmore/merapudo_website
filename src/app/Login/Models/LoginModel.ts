export class LoginRequestModel {

    public user_name: string;
    public user_pass: string;
    constructor(values: Object = {}) {
        Object.assign(this, values);
    }

}