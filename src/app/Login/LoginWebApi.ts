import 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions } from '@angular/http';
import { HttpClient } from "@angular/common/http"
import { Observable } from 'rxjs';
import { LoginURLConstants } from './LoginURLConstants';
@Injectable()
export class LoginAPICalls {
    constructor(private http: HttpClient) { }

    loginUser(data): Observable<any> {
        return this.http.post(LoginURLConstants.checkUserLogin, data)
    }
}