import { Component, OnInit } from '@angular/core';
import { DashboardController } from './DashboardController';
import { DashboardURLConstants } from './DashboardURLConstants';
import { DashboardAPICalls } from './DashboardWebApi';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { variable } from '@angular/compiler/src/output/output_ast';
import { DashboardRequestModel } from './Models/DashboardModel';
declare var $: any;
@Component({
    selector: 'app-root',
    templateUrl: '../../assets/template/dashboard.html',
    styleUrls: ['../../assets/css/dashboard.css'],
    providers: [DashboardAPICalls]
})
export class DashboardMain implements OnInit {
    private dashboardController: DashboardController;
    public dashboardRequestModel: any;
    constructor(private dashboardWebApi: DashboardAPICalls) { }

    ngOnInit() {

    }


}