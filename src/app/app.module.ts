import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LoginMain } from './Login/LoginMain';
import { DashboardMain } from './Dashboard/DashboardMain'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
const appRoutes: Routes =
  [
    { path: '', component: LoginMain },
    { path: 'dashboard', component: DashboardMain }
  ];
@NgModule({
  declarations: [
    AppComponent,
    LoginMain,
    DashboardMain
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule, HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
